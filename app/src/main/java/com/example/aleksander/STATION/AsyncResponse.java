package com.example.aleksander.STATION;

import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Aleksander on 02-Nov-17.
 */

public interface AsyncResponse {
    void processFinish(List<Marker> listOfMarker, List<HashMap<String, String>> list);
}
