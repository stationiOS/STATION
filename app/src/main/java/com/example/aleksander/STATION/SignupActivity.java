package com.example.aleksander.STATION;

/**
 * Created by Aleksander on 18-Oct-17.
 */

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class SignupActivity extends AsyncTask<String, Void, String> {

    private Context context;
    private String whichPHP;

    public SignupActivity(Context context, String whichPHP) {
        this.context = context;
        this.whichPHP = whichPHP;
    }

    protected void onPreExecute() {

    }

    @Override
    protected String doInBackground(String... arg0) {
        String name = arg0[0];
        String number;
        try{
            number = arg0[1];
        }
        catch(ArrayIndexOutOfBoundsException e){
            number = "0";
        }


        String link;
        String data;
        BufferedReader bufferedReader;
        String result;

        try {
            data = "?name=" + URLEncoder.encode(name, "UTF-8");
            data += "&number=" + URLEncoder.encode(number, "UTF-8");

            link = "https://station.000webhostapp.com/"+whichPHP + data;
            URL url = new URL(link);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            result = bufferedReader.readLine();
            return result;
        } catch (Exception e) {
            return "Exception: " + e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String result) {
        //TODO maybe do something with this later
    }
}